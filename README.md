## ===
### This is old version of research abandoned for [matrix_implementations_in_erlang](https://gitlab.com/flmath/matrix_implementations_in_erlang)
### Main reason why project is moved is that fprof occured to crash during stress test with big matrices. So I decided to use timer:tc, but still I am leaving legacy version since fprof provides more infomation (number of function calls) than tc.
## ===

The erlang and OTP are lacking explicite matrix support.

This is a little study of possible implementations for matrixes in Erlang. 

Every implementation contains rows_sum/1, cols_sum/1, get_value/3 and set_value/4 functions.

To run all unit tests use:
 
 > make test

To run some test and store processed results, for example:

 > ./fprof_escript all get_value 20 20 > temp

Get markdown GFM table:

 > cat temp | grep '>>>' | cut -d ' ' -f 2-

